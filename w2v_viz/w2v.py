# coding=utf-8
import gensim
# http://radimrehurek.com/gensim/models/word2vec.html
# to install gensim we need a fortran compiler -> brew install gmp or brew install gcc in the worst scenario
# basically, it is scipy that needs fortran
# https://rare-technologies.com/word2vec-tutorial/
# size: the size of the NN layers, which correspond to the “degrees” of freedom the training algorithm has
# workers number means parallelization
model = gensim.models.Word2Vec([], min_count=1, size=100, workers=4)

# or

model = gensim.models.Word2Vec(["one sentence", "second sentence", "i love math", "mpla sentence", "second", "i love teach"], iter=1)  # an empty model, no training yet
model.build_vocab(["one sentence", "second sentence", "i love math", "mpla sentence", "second", "i love teach"])  # can be a non-repeatable, 1-pass generator
model.train(["one sentence", "second sentence", "i love math"])  # can be a non-repeatable, 1-pass generator

print model.similar_by_word("i love math")

# # saving and loading
model.save('/tmp/mymodel')
new_model = gensim.models.Word2Vec.load('/tmp/mymodel')
model = gensim.models.Word2Vec.load_word2vec_format('/tmp/vectors.txt', binary=False)
# using gzipped/bz2 input works too, no need to unzip:
model = gensim.models.Word2Vec.load_word2vec_format('/tmp/vectors.bin.gz', binary=True)

model = gensim.models.Word2Vec.load('/tmp/mymodel')
model.train(["more sentences"])